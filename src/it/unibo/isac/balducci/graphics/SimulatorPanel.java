package it.unibo.isac.balducci.graphics;

import it.unibo.isac.balducci.common.Pos2d;
import it.unibo.isac.balducci.model.SimulatorObj;
import it.unibo.isac.balducci.model.World;

import java.awt.*;

import javax.swing.JPanel;
import javax.swing.SwingUtilities;
import javax.swing.border.*;

public class SimulatorPanel extends JPanel{

	private int rows;
	private int cols;
	private int w,h;
	private Cell[][] cells;
	World world;
	Dimension cellDim;
	
	public SimulatorPanel(int w, int h, World world){
		setBackground(Color.GRAY);
		setLayout(new GridBagLayout());
		
		this.w=w;
		this.h=h;
		
		rows=world.getWorldHeight();
		cols=world.getWorldWidth();	
		cells = new Cell[rows][cols];
		cellDim = new Dimension(w/cols,h/rows);
		this.world=world;
		
        GridBagConstraints gbc = new GridBagConstraints();
        for (int i = 0; i < rows; i++) {
            for (int j = 0; j < cols; j++) {
                gbc.gridx = j; 
                gbc.gridy = i;

                Cell cell = new Cell();
                Border border = null;
                if (i < rows-1) {
                    if (j < cols-1) {
                        border = new MatteBorder(1, 1, 0, 0, Color.GRAY);
                    } else {
                        border = new MatteBorder(1, 1, 0, 1, Color.GRAY);
                    }
                } else {
                    if (j < cols-1) {
                        border = new MatteBorder(1, 1, 1, 0, Color.GRAY);
                    } else {
                        border = new MatteBorder(1, 1, 1, 1, Color.GRAY);
                    }
                }
                cell.setBorder(border);
                cell.setPreferredSize(cellDim);
                add(cell, gbc);
                cells[i][j]=cell;
            }            
        }		
	}
	

	
	
	@Override
	public void paint(Graphics g) {
		//System.out.println(SwingUtilities.isEventDispatchThread());
		Cell cell;	
		Pos2d agentPos = world.getAgent().getPos();
        for (int i = 0; i < rows; i++) {
            for (int j = 0; j < cols; j++) {
            	//Draw borders
            	cell= cells[i][j];                
                Border border = null;
                if (i < rows-1) {
                    if (j < cols-1) {
                        border = new MatteBorder(1, 1, 0, 0, Color.GRAY);
                    } else {
                        border = new MatteBorder(1, 1, 0, 1, Color.GRAY);
                    }
                } else {
                    if (j < cols-1) {
                        border = new MatteBorder(1, 1, 1, 0, Color.GRAY);
                    } else {
                        border = new MatteBorder(1, 1, 1, 1, Color.GRAY);
                    }
                }
                cell.setBorder(border);
                cells[i][j]=cell;
                
                
                //Draw simulator objects
                if(agentPos.getRow()==i && agentPos.getCol()==j){
                	world.getAgent().draw(cell);
                }
                else{
	                world.getSimulatorObj(i,j).draw(cell);
                }
            }            
        }
	}



	public class Cell extends JPanel {
		
        
    }
}
