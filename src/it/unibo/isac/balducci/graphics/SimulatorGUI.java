package it.unibo.isac.balducci.graphics;

import it.unibo.isac.balducci.model.World;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;

import javax.swing.*;

public class SimulatorGUI extends JFrame{

	SimulatorPanel simulatorPanel;
	GUIListener guiListener;
	
	private int w,h;
	private String path="";
	
	private JButton btnStart;
	private JButton btnStop;
	private JButton btnPause;
	private JButton btnBrowseScenario;
	
	public SimulatorGUI(int w,int h, GUIListener guiListener){
		super("Empowerment Simulator");
		this.guiListener=guiListener;
		initComponents(w,h);
	}
	
	private void initComponents(int w, int h){
		this.w=w;
		this.h=h;
		setSize(w,h);
		setMinimumSize(new Dimension(w,h));
		setLocation(100,100);
		setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        setResizable(false);        
        
        //Initialize an empty SimulatorPanel
		/*simulatorPanel = new SimulatorPanel(w,h);
		getContentPane().add(simulatorPanel, BorderLayout.CENTER);*/
		
		JPanel buttonsPanel = new JPanel(new GridBagLayout());
		GridBagConstraints c = new GridBagConstraints();

		c.insets=new Insets(5,10,5,10); //Sopra, Destra, Basso, Sinistra
	
		
		btnStart = new JButton("Start");
		btnStart.addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(java.awt.event.ActionEvent evt) {
					startButtonActionPerformed(evt);
			}
		});
		c.gridx=0;
		c.gridy=0;	
		buttonsPanel.add(btnStart, c);
		
		btnStop = new JButton("Stop");
		btnStop.setEnabled(false);
		btnStop.addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(java.awt.event.ActionEvent evt) {
					stopButtonActionPerformed(evt);
			}
		});
		c.gridx=1;
		c.gridy=0;	
		buttonsPanel.add(btnStop, c);
		
		btnPause = new JButton("Pause");
		btnPause.setEnabled(false);
		btnPause.addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(java.awt.event.ActionEvent evt) {
					pauseButtonActionPerformed(evt);
			}
		});
		c.gridx=2;
		c.gridy=0;	
		buttonsPanel.add(btnPause, c);
		
		btnBrowseScenario = new JButton("Scenario");
		btnBrowseScenario.addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(java.awt.event.ActionEvent evt) {
				browseScenarioButtonActionPerformed(evt);
			}
		});
		c.gridx=3;
		c.gridy=0;	
		buttonsPanel.add(btnBrowseScenario);
		
		getContentPane().add(buttonsPanel,BorderLayout.SOUTH);
		
		pack();
		setVisible(true);		
	}
	
	public void initSimulatorPanel(World world){
		simulatorPanel = new SimulatorPanel(w,h,world);
		getContentPane().add(simulatorPanel,BorderLayout.CENTER);
		pack();
	}
	
	public void render(){
		try {
			//TODO invokeAndWait o invokeLater??
	    	SwingUtilities.invokeAndWait(() -> {
	    		simulatorPanel.repaint();
	    	});
    	} catch (Exception ex){
    		ex.printStackTrace();
    	}
	}
	
	private void startButtonActionPerformed(java.awt.event.ActionEvent evt){
		if(btnStop.isEnabled()){
			btnStart.setEnabled(false);
			btnPause.setEnabled(true);
			guiListener.resumed();
		}
		else{
			btnStart.setEnabled(false);
			btnStop.setEnabled(true);
			btnPause.setEnabled(true);
			guiListener.started(path);			
		}
	}
	
	private void stopButtonActionPerformed(java.awt.event.ActionEvent evt){
		btnStart.setEnabled(true);
		btnStop.setEnabled(false);
		btnPause.setEnabled(false);
		guiListener.stopped();
	}
	
	private void pauseButtonActionPerformed(java.awt.event.ActionEvent evt){
		btnPause.setEnabled(false);
		btnStart.setEnabled(true);
		guiListener.paused();
	}
	
	private void  browseScenarioButtonActionPerformed(java.awt.event.ActionEvent evt) {
    	final JFileChooser fc = new JFileChooser(System.getProperty("user.dir"));
    	int returnVal = fc.showOpenDialog(this);
        if (returnVal == JFileChooser.APPROVE_OPTION) {
            path = fc.getSelectedFile().getAbsolutePath();
        }
    }
}
