package it.unibo.isac.balducci.graphics;

public interface GUIListener {

	void started(String path);
	
	void stopped();
	
	void paused();
	
	void resumed();
}
