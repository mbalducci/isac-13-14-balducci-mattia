package it.unibo.isac.balducci.monitors;

public class SyncMonitor {

	int goal;
	int counter;
	boolean goOn;
	
	public SyncMonitor(int goal){
		this.goal=goal;
		goOn=false;
	}
	
	public synchronized void done(){
		counter++;
		if(counter==goal){
			counter=0;
			goOn=true;
			notifyAll();
		}
	}
	
	public synchronized void waiForAll() throws InterruptedException {
		while(!goOn){
			wait();
		}
		goOn=false;
	}
	
}
