package it.unibo.isac.balducci.monitors;

public class PauseMonitor {

	boolean pause;
	
	public PauseMonitor(){
		pause=false;
	}
	
	public synchronized void pause(){
		pause=true;
	}
	
	public synchronized void resume(){
		pause=false;
		notify();
	}
	
	public synchronized void isPaused(){
		while(pause==true){
			try {
				wait();
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}
	}
}
