package it.unibo.isac.balducci.empowerment;

import it.unibo.isac.balducci.core.FileLogger;
import it.unibo.isac.balducci.model.SimulatorObj;
import it.unibo.isac.balducci.model.World;
import it.unibo.isac.balducci.monitors.SyncMonitor;

import org.apache.commons.lang3.SerializationUtils;

import java.util.ArrayList;
import java.util.Random;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class EmpowermentCalculator {

	private World world,testWorld;
	private ExecutorService executor;
	private int nThreads;
	private int nSamples;
	private int taskSamples;
	private int sequenceLength;
	private boolean[][] reachedPositions;
	private SyncMonitor sync;
	private int nCapabilities;
	private Double[] empowerments;

	
	public EmpowermentCalculator(int nSamples, int sequenceLength, World world){
		this.nSamples=nSamples;
		this.sequenceLength=sequenceLength;
		this.world=world;		
		nThreads = Runtime.getRuntime().availableProcessors() + 1;
		sync= new SyncMonitor(nThreads);
		nCapabilities=world.getAgent().getCapabilities().size();
		empowerments= new Double[nCapabilities];
		
	}
	
	public int calc(){
		SimulatorObj agent;
		
		executor = Executors.newFixedThreadPool(nThreads);
		taskSamples = (int)(nSamples/nThreads);
		
		for(int i=0; i<nCapabilities; i++){
			
			//Perform the action to test
			testWorld=SerializationUtils.clone(world);
			
			agent=testWorld.getAgent();
			agent.getCapabilities().get(i).doAction(testWorld, agent);
			agent.applyPhysics(testWorld);
			testWorld.updateWorldState();
			reachedPositions=new boolean[world.getWorldHeight()][world.getWorldWidth()];
			//Execute tasks to evaluate the number of different states reachable with nSamples action sequences
			for(int j=0; j<nThreads; j++){
				executor.execute(new SparseSamplingEmpowerment(sequenceLength,taskSamples,testWorld,reachedPositions,sync));
			}
			
			try {
				sync.waiForAll();
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
			
			
			//executor.shutdown();
			
			empowerments[i]=calculateEmpowerment();
		}
		executor.shutdown();
		int choosenAction=chooseAction();
		FileLogger.getInstance().log(""+empowerments[choosenAction]+"\n");
		System.out.println("Action "+choosenAction+" has been choosen, with an empowerment of "+empowerments[choosenAction]);
		return choosenAction;
	
	}
	
	//Count the different reached states and evaluates the Empowerment value (simply the log)
	private double calculateEmpowerment(){
		int diffStates=0;
		for(int i=0; i<world.getWorldHeight(); i++ ){
			for(int j=0; j<world.getWorldWidth(); j++){
				if(reachedPositions[i][j]){
					diffStates++;
				}
			}
		}
		return Math.log(diffStates);
	}
	
	private int chooseAction(){
		ArrayList<Integer> bestActions = new ArrayList<Integer>();
		double best=-1;
		for(int i=0; i<nCapabilities; i++){
			if(empowerments[i]>best){
				bestActions= new ArrayList<Integer>();
				bestActions.add(i);
				best=empowerments[i];
			}
			else if(empowerments[i]==best){
				bestActions.add(i);
			}
		}
		
		Random gen = new Random();
		int choosenAction = bestActions.get(gen.nextInt(bestActions.size()));
		return choosenAction;
	}
}
