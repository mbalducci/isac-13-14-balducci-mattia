package it.unibo.isac.balducci.empowerment;

import java.util.ArrayList;
import java.util.Random;

import org.apache.commons.lang3.SerializationUtils;

import it.unibo.isac.balducci.capabilities.Capability;
import it.unibo.isac.balducci.common.Pos2d;
import it.unibo.isac.balducci.model.SimulatorObj;
import it.unibo.isac.balducci.model.World;
import it.unibo.isac.balducci.monitors.SyncMonitor;

public class SparseSamplingEmpowerment implements Runnable {

	//Length of the action sequence to evaluate
	int n;
	//Number of samples to do
	int nSamples;
	World originalWorld, testWorld;
	boolean[][] reachedPositions;
	SimulatorObj agent;
	ArrayList<Capability> capabilities;
	SyncMonitor sync;
	
 
	public SparseSamplingEmpowerment(int n, int nSamples, World world, boolean[][] reachedPositions,SyncMonitor sync){
		this.n = n;
		this.nSamples=nSamples;
		this.originalWorld=world;
		this.reachedPositions=reachedPositions;
		this.sync=sync;
	}
	
	
	@Override
	public void run() {
		Random gen = new Random();
		
		//Initial position count as reached state
		agent=originalWorld.getAgent();
		Pos2d pos=agent.getPos();
		reachedPositions[pos.getRow()][pos.getCol()]=true;
		
		//Index of the random choosen action
		int action;		
		for(int i=0; i<nSamples; i++){
			testWorld= SerializationUtils.clone(originalWorld);
			agent=testWorld.getAgent();
			capabilities = agent.getCapabilities();
			for(int j=0; j<n; j++){
				action=gen.nextInt(capabilities.size());
				capabilities.get(action).doAction(testWorld, agent);
				agent.applyPhysics(testWorld);
				testWorld.updateWorldState();
			}
			pos=agent.getPos();
			reachedPositions[pos.getRow()][pos.getCol()]=true;
		}
		sync.done();
	}
	
}
