package it.unibo.isac.balducci.model;

import java.io.Serializable;

import it.unibo.isac.balducci.common.Pos2d;

public class World implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	SimulatorObj[][] worldGrid;
	NormalAgent agent;
	private int rows,cols;
	
	public World(int rows, int cols){
		this.rows = rows;
		this.cols = cols;
		worldGrid = new SimulatorObj[rows][cols];
 	} 
	
	
	public void doOneStep(){
		agent.update(this);
		updateWorldState();			
	}
	
	public void updateWorldState(){
		for(int i=0; i<rows; i++){
			for(int j=0; j<cols; j++){
				//if(worldGrid[i][j].getType() != SimulatorObj.Type.AGENT){
					worldGrid[i][j].update(this);
				//}
			}
		}	
	}
	
	public int getWorldWidth(){
		return cols;
	}
	
	public int getWorldHeight(){
		return rows;
	}
	
	public void setSimulatorObj(Pos2d pos, SimulatorObj obj){
		worldGrid[pos.getRow()][pos.getCol()]= obj;
	}	
	
	public void setAgent(NormalAgent obj){
		agent=obj;
	}	
	
	public SimulatorObj getAgent(){
		return agent;
	}
	
	public SimulatorObj getSimulatorObj(Pos2d pos){
		return worldGrid[pos.getRow()][pos.getCol()];
	}
	
	
	public SimulatorObj getSimulatorObj(int row, int col){
		if(row>=0 && row<rows && col>=0 && col<cols  ){
			return worldGrid[row][col];
		}
		return null;
	}

	
}
