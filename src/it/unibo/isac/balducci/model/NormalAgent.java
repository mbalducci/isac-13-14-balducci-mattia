package it.unibo.isac.balducci.model;

import java.awt.Color;
import java.util.Random;

import javax.swing.JPanel;

import it.unibo.isac.balducci.common.Config;
import it.unibo.isac.balducci.common.Pos2d;
import it.unibo.isac.balducci.empowerment.EmpowermentCalculator;

public class NormalAgent extends SimulatorObj{

	boolean empty;
	
	public NormalAgent (Pos2d pos){
		super(pos);
		type = SimulatorObj.Type.AGENT;
		empty=true;
	}

	@Override
	protected void makeAMove(World world) {
		int seqLength;
		/*There is a SEQUENCE_LENGTH_RANDOM_PROBL probability that the sequence length will be changed from the
		 * defined value to a random generated (uniform distribution) value SEQUENCE_MIN_LENGTH <= X < SEQUENCE_LENGTH
		 */
		
		if(Math.random() < Config.SEQUENCE_LENGTH_RANDOM_PROBL){
			Random gen = new Random();
			seqLength = gen.nextInt(Config.SEQUENCE_LENGTH-Config.SEQUENCE_MIN_LENGTH)+Config.SEQUENCE_MIN_LENGTH;
			System.out.println("La seq random generata � : "+seqLength);
		}
		else{
			seqLength=Config.SEQUENCE_LENGTH;
		}
		EmpowermentCalculator ec=new EmpowermentCalculator(Config.N_SAMPLES,seqLength,world);
		int action=ec.calc();
		capabilities.get(action).doAction(world, this);
	}

	
	@Override
	public void draw(JPanel cell) {
		cell.setBackground(Color.BLACK);		
	}


	
	public boolean isEmpty(){
		return empty;
	}
	
	public void pickUp(){
		empty=false;
	}
	
	public void drop(){
		empty=true;
	}
}
