package it.unibo.isac.balducci.model;

import java.io.Serializable;
import java.util.ArrayList;

import javax.swing.JPanel;

import it.unibo.isac.balducci.capabilities.Capability;
import it.unibo.isac.balducci.common.Pos2d;
import it.unibo.isac.balducci.physics.PhysicsComponent;

public abstract class SimulatorObj implements Serializable{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public static enum Type { AGENT, WATER, TERRAIN, AIR }
	
	Pos2d pos;
	Type type;
	ArrayList<Capability> capabilities;
	ArrayList<PhysicsComponent> phys;
	
	public SimulatorObj(Pos2d pos){
		this.pos=pos;
		capabilities=new ArrayList<Capability>();
		phys=new ArrayList<PhysicsComponent>();
	}
	
	public Pos2d getPos(){
		return pos;
	}
	
	public void setPos(Pos2d pos){
		this.pos=pos;
	}
	
	public Type getType(){
		return type;
	}
	
	public void addCapability(Capability c){
		capabilities.add(c);
	}
	
	public ArrayList<Capability> getCapabilities(){
		return capabilities;
	}
	
	public void addPhysics(PhysicsComponent p){
		phys.add(p);
	}
	
	public ArrayList<PhysicsComponent> getPhysicsComponents(){
		return phys;
	}
	
	public void update(World world){
		makeAMove(world);
		applyPhysics(world);
	}
	
	protected abstract void makeAMove(World world);
	
	public void applyPhysics(World world){
		for(int i=0; i<phys.size(); i++){
			phys.get(i).applyPhysics(world, this);
		}
	}
	
	public abstract void draw(JPanel cell); //TODO capire come fare e cosa passargli
}
