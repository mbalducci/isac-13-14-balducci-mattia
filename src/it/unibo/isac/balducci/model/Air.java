package it.unibo.isac.balducci.model;

import java.awt.Color;

import javax.swing.JPanel;

import it.unibo.isac.balducci.common.Pos2d;

public class Air extends SimulatorObj{

	public Air(Pos2d pos) {
		super(pos);
		type= SimulatorObj.Type.AIR;
	}



	@Override
	public void draw(JPanel cell) {
		cell.setBackground(Color.CYAN);		
	}



	@Override
	protected void makeAMove(World world) {
		//do nothing
		
	}



	
}
