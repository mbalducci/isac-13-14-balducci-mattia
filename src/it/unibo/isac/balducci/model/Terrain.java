package it.unibo.isac.balducci.model;

import it.unibo.isac.balducci.common.Pos2d;

import java.awt.Color;

import javax.swing.JPanel;

public class Terrain extends SimulatorObj{
	
	public Terrain(Pos2d pos) {
		super(pos);
		type= SimulatorObj.Type.TERRAIN;
	}

	

	@Override
	public void draw(JPanel cell) {
		cell.setBackground(Color.GRAY);		
	}



	@Override
	protected void makeAMove(World world) {
		//do nothing
		
	}




}
