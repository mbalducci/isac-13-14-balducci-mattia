package it.unibo.isac.balducci.model;

import it.unibo.isac.balducci.common.Pos2d;

import java.awt.Color;

import javax.swing.JPanel;

public class Water extends SimulatorObj{

	public Water(Pos2d pos) {
		super(pos);
		type= SimulatorObj.Type.WATER;
	}


	
	@Override
	public void draw(JPanel cell) {
		cell.setBackground(Color.BLUE);		
	}



	@Override
	protected void makeAMove(World world) {
		//do nothing
		
	}

}
