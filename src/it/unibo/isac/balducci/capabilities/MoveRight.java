package it.unibo.isac.balducci.capabilities;

import it.unibo.isac.balducci.common.Pos2d;
import it.unibo.isac.balducci.model.*;

/* Represents the capability to move 1 square right if the destination square is free (AIR)
 * 
 *  Can be used only by a NormalAgent
 * */

public class MoveRight extends AgentCapability{

	@Override
	public void doAction(World world, SimulatorObj obj) {
		if(isInWater(world,obj))
			return;
		
		Pos2d pos=obj.getPos();
		if(world.getSimulatorObj(pos.getRow(),pos.getCol()+1) != null 
				&& world.getSimulatorObj(pos.getRow(),pos.getCol()+1).getType() != SimulatorObj.Type.TERRAIN){
			//Pos2d oldPos = pos;
			pos = new Pos2d(pos.getRow(), pos.getCol()+1);
			obj.setPos(pos);
			//world.setSimulatorObj(pos, obj);
			//world.setSimulatorObj(oldPos, new Air(oldPos));
		}		
		
	}

}
