package it.unibo.isac.balducci.capabilities;

import it.unibo.isac.balducci.common.Pos2d;
import it.unibo.isac.balducci.model.Air;
import it.unibo.isac.balducci.model.SimulatorObj;
import it.unibo.isac.balducci.model.World;

/* Represents the capability to move 1 square right, can climb up to 1 square of TERRAIN
 * 
 *  Can be used only by a NormalAgent
 * */

public class MoveRightClimb extends AgentCapability{
	@Override
	public void doAction(World world, SimulatorObj obj) {
		if(isInWater(world,obj))
			return;
		
		Pos2d pos=obj.getPos();
		if(world.getSimulatorObj(pos.getRow(),pos.getCol()+1) != null 
				&& world.getSimulatorObj(pos.getRow(),pos.getCol()+1).getType() != SimulatorObj.Type.TERRAIN){
			//Pos2d oldPos = pos;
			pos = new Pos2d(pos.getRow(), pos.getCol()+1);
			obj.setPos(pos);
			//world.setSimulatorObj(pos, obj);
			//world.setSimulatorObj(oldPos, new Air(oldPos));
		}else if(world.getSimulatorObj(pos.getRow(),pos.getCol()+1) != null
					&& world.getSimulatorObj(pos.getRow()-1,pos.getCol()+1) != null 
					&& world.getSimulatorObj(pos.getRow(),pos.getCol()+1).getType() == SimulatorObj.Type.TERRAIN
					&& world.getSimulatorObj(pos.getRow()-1,pos.getCol()+1).getType() != SimulatorObj.Type.TERRAIN){
			//Pos2d oldPos = pos;
			pos = new Pos2d(pos.getRow()-1, pos.getCol()+1);
			obj.setPos(pos);
			//world.setSimulatorObj(pos, obj);
			//world.setSimulatorObj(oldPos, new Air(oldPos));
			
		}	
	}
}
