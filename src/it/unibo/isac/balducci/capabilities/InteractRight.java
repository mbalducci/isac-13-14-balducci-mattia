package it.unibo.isac.balducci.capabilities;

import it.unibo.isac.balducci.common.Pos2d;
import it.unibo.isac.balducci.core.SimulatorFactory;
import it.unibo.isac.balducci.model.Air;
import it.unibo.isac.balducci.model.NormalAgent;
import it.unibo.isac.balducci.model.SimulatorObj;
import it.unibo.isac.balducci.model.World;

/* Represents the capability to pick up and drop a TERRAIN in the square at the right of the Agent
 * If the Agent's inventory is empty work as pick up, if not drop a TERRAIN
 *  Can be used only by a NormalAgent
 * */

public class InteractRight extends AgentCapability {

	@Override
	public void doAction(World world, SimulatorObj obj) {
		if(isInWater(world,obj))
			return;
		SimulatorFactory f = SimulatorFactory.getInstance();
		Pos2d pos=obj.getPos();
		
		if(obj.getType()==SimulatorObj.Type.AGENT){
			NormalAgent agent=(NormalAgent)obj;
			
			if(world.getSimulatorObj(pos.getRow(),pos.getCol()+1) != null){
				//If empty -> pickUp
				if(agent.isEmpty() &&
						world.getSimulatorObj(pos.getRow(),pos.getCol()+1).getType() == SimulatorObj.Type.TERRAIN){			
					agent.pickUp();
					pos=new Pos2d(pos.getRow(),pos.getCol()+1);
					world.setSimulatorObj(pos, new Air(pos));								
				}
				//If not empty -> drop
				else if(!agent.isEmpty() &&
						world.getSimulatorObj(pos.getRow(),pos.getCol()+1).getType() == SimulatorObj.Type.AIR){
					agent.drop();
					pos=new Pos2d(pos.getRow(),pos.getCol()+1);
					world.setSimulatorObj(pos, f.terrain(pos));
				}
			}
		}
		
	}
}
