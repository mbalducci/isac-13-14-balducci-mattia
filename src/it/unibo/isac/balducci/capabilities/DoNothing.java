package it.unibo.isac.balducci.capabilities;

import it.unibo.isac.balducci.model.*;

/* Represents the capability to do nothing 
 * */

public class DoNothing extends AgentCapability{

	@Override
	public void doAction(World world, SimulatorObj obj) {
		// doNothing
		
	}
	

}
