package it.unibo.isac.balducci.capabilities;

import java.io.Serializable;

import it.unibo.isac.balducci.model.SimulatorObj;
import it.unibo.isac.balducci.model.World;

public interface Capability extends Serializable{

	public void doAction(World world, SimulatorObj obj);
}
