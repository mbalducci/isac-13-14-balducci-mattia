package it.unibo.isac.balducci.capabilities;

import it.unibo.isac.balducci.common.Pos2d;
import it.unibo.isac.balducci.model.*;

/* Represents the capability to pick up a TERRAIN from the square at the right of the Agent
 * 
 *  Can be used only by a NormalAgent
 * */

public class PickUpRight extends AgentCapability{

		@Override
		public void doAction(World world, SimulatorObj obj) {
			if(isInWater(world,obj))
				return;
			
			Pos2d pos=obj.getPos();			
			if(obj.getType()==SimulatorObj.Type.AGENT){
				NormalAgent agent=(NormalAgent)obj;
				if(world.getSimulatorObj(pos.getRow(),pos.getCol()+1) != null 
						&& world.getSimulatorObj(pos.getRow(),pos.getCol()+1).getType() == SimulatorObj.Type.TERRAIN){
					if(agent.isEmpty()){
						agent.pickUp();
						pos=new Pos2d(pos.getRow(),pos.getCol()+1);
						world.setSimulatorObj(pos, new Air(pos));
					}
				}
			}
			
		}	

}
