package it.unibo.isac.balducci.capabilities;

import it.unibo.isac.balducci.model.SimulatorObj;
import it.unibo.isac.balducci.model.World;

public abstract class AgentCapability implements Capability{

	
	public boolean isInWater(World world, SimulatorObj obj){
		if(world.getSimulatorObj(obj.getPos()).getType()== SimulatorObj.Type.WATER){
			return true;
		}
		return false;
	}
}
