package it.unibo.isac.balducci.capabilities;

import it.unibo.isac.balducci.common.Pos2d;
import it.unibo.isac.balducci.core.SimulatorFactory;
import it.unibo.isac.balducci.model.NormalAgent;
import it.unibo.isac.balducci.model.SimulatorObj;
import it.unibo.isac.balducci.model.Terrain;
import it.unibo.isac.balducci.model.World;

/* Represents the capability to drop a TERRAIN in the square at the right of the Agent
 * 
 *  Can be used only by a NormalAgent
 * */

public class DropRight extends AgentCapability{

	@Override
	public void doAction(World world, SimulatorObj obj) {
		if(isInWater(world,obj))
			return;
		SimulatorFactory f = SimulatorFactory.getInstance();
		Pos2d pos=obj.getPos();
		
		if(obj.getType()==SimulatorObj.Type.AGENT){
			NormalAgent agent=(NormalAgent)obj;
			if(world.getSimulatorObj(pos.getRow(),pos.getCol()+1) != null 
					&& world.getSimulatorObj(pos.getRow(),pos.getCol()+1).getType() == SimulatorObj.Type.AIR){
				if(!agent.isEmpty()){
					agent.drop();
					pos=new Pos2d(pos.getRow(),pos.getCol()+1);
					world.setSimulatorObj(pos, f.terrain(pos));
				}
			}
		}
		
	}
}
