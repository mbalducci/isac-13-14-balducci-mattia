package it.unibo.isac.balducci.capabilities;

import it.unibo.isac.balducci.common.Pos2d;
import it.unibo.isac.balducci.model.SimulatorObj;
import it.unibo.isac.balducci.model.World;

/* Represents the capability to move 1 square down (used only for flying agent) if the destination square is free (AIR)
 * 
 *  Can be used only by a NormalAgent
 * */

public class MoveDown extends AgentCapability{

	@Override
	public void doAction(World world, SimulatorObj obj) {
		if(isInWater(world,obj))
			return;
		
		Pos2d pos=obj.getPos();
		if(world.getSimulatorObj(pos.getRow()+1,pos.getCol()) != null 
				&& world.getSimulatorObj(pos.getRow()+1,pos.getCol()).getType() != SimulatorObj.Type.TERRAIN){
			pos = new Pos2d(pos.getRow()+1, pos.getCol());
			obj.setPos(pos);
		}		
	}
}
