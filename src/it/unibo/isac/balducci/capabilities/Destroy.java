package it.unibo.isac.balducci.capabilities;

import it.unibo.isac.balducci.model.*;

/* Represents the capability to destroy a SimulatorObj
 * 
 * Can be used only by a NormaAgent
 * */

public class Destroy extends AgentCapability{


	
	@Override
	public void doAction(World world, SimulatorObj obj) {
		if(isInWater(world,obj))
			return;
		
		if(obj.getType() == SimulatorObj.Type.AGENT){
			NormalAgent agent = (NormalAgent)obj;
			if(!agent.isEmpty()){
				agent.drop();
			}
		}	
	}

}
