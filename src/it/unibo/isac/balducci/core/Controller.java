package it.unibo.isac.balducci.core;

import java.io.BufferedReader;
import java.io.DataInputStream;
import java.io.FileInputStream;
import java.io.InputStreamReader;
import java.util.Arrays;

import it.unibo.isac.balducci.common.Config;
import it.unibo.isac.balducci.common.Pos2d;
import it.unibo.isac.balducci.exceptions.InvalidDataFileException;
import it.unibo.isac.balducci.graphics.GUIListener;
import it.unibo.isac.balducci.graphics.SimulatorGUI;
import it.unibo.isac.balducci.model.NormalAgent;
import it.unibo.isac.balducci.model.SimulatorObj;
import it.unibo.isac.balducci.model.World;

/*
 * Simulation controller, instantiates and initializes the SimulatorGUI and the SimulatorEngine.
 * Act as listener of the Start,Stop, Pause and Resume Gui events.
 */

public class Controller implements GUIListener{

	SimulatorGUI scene;
	SimulatorEngine simEngine;
	
	
	public Controller() {
		scene = new SimulatorGUI(900,600,this);
	}

	@Override
	public void started(String path) {
		World world = createWorldFromFile(path);
		scene.initSimulatorPanel(world);
		simEngine = new SimulatorEngine(Config.N_ITERATIONS,scene,world);
		simEngine.start();
		
	}

	
	/*
	 * Instantiate the world grid 
	 */
	private World createWorldFromFile(String path){
		World world = new World(1,1);
		SimulatorFactory f = SimulatorFactory.getInstance();
		
		
		try {
			FileInputStream fstream = new FileInputStream(path);
			DataInputStream in = new DataInputStream(fstream);
			BufferedReader br = new BufferedReader(new InputStreamReader(in));
			String strLine;

			//Read number of rows
			if ((strLine = br.readLine()) == null) {
				br.close(); 
				throw new InvalidDataFileException("data file is empty");
			}
			int rows = Integer.parseInt((strLine.split("="))[1]);
			
			//Read number of columns
			if ((strLine = br.readLine()) == null) {
				br.close(); 
				throw new InvalidDataFileException("data file is incomplete");
			}
			int cols = Integer.parseInt((strLine.split("="))[1]);
			
			world = new World(rows,cols);
			
			String[] simulatorObjects = new String[cols];			
			int filledRows=0;
			Pos2d pos;
			SimulatorObj obj;
			
			//Read the numbers line used as column reference (useless information)
			br.readLine();
			
			//For each row, create and set a SimulatorObj as defined in the configuration file
			while ((strLine = br.readLine()) != null && filledRows < rows) {				
				try {
					simulatorObjects = strLine.split(" ");
					for(int j=0; j<cols; j++){
						pos= new Pos2d(filledRows,j);
						obj = f.createSimulatorObj(simulatorObjects[j], pos);
						/*If the SimulatorObj is an Agent put an AIR in the grid and set the "agent" field of the world 
						  (an agent moves over the grid)
						 */
						if(obj.getType()==SimulatorObj.Type.AGENT){
							world.setAgent((NormalAgent)obj);
							world.setSimulatorObj(pos, f.air(new Pos2d(filledRows,j)));
						}
						else{
							world.setSimulatorObj(pos, obj);
						}
					}					
					filledRows++;
				} catch (Exception e1) {
					br.close();
					throw new InvalidDataFileException("data file is incomplete");
				}
			}

			//Check that all expected rows has been read
			if (filledRows != rows) {
				br.close();
				throw new InvalidDataFileException("expected " + rows
						+ " rows, but found only" + filledRows + ".");
			}

			in.close();
		} catch (Exception e) {// Catch exception if any
			System.err.println("Error loading data: " + e.getMessage());
		}	
		
		return world;
	}
	
	
	@Override
	public void stopped() {
		simEngine.terminate();
	}

	@Override
	public void paused() {
		simEngine.pauseSimulation();
		
	}

	@Override
	public void resumed() {
		simEngine.resumeSimulation();		
	}

}
