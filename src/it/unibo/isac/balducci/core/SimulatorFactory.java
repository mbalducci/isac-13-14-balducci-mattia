package it.unibo.isac.balducci.core;

import it.unibo.isac.balducci.capabilities.*;
import it.unibo.isac.balducci.common.Config;
import it.unibo.isac.balducci.common.Pos2d;
import it.unibo.isac.balducci.exceptions.InvalidDataFileException;
import it.unibo.isac.balducci.model.*;
import it.unibo.isac.balducci.physics.*;


public class SimulatorFactory {

	static private SimulatorFactory instance;
	
	static public SimulatorFactory getInstance(){
		if (instance == null){
			instance = new SimulatorFactory();
		}
		return instance;
	}
	
	public SimulatorObj createSimulatorObj(String obj, Pos2d pos) throws InvalidDataFileException{
		switch(obj){						
			case "A":
				return air(pos);
			case "T":
				return terrain(pos);
			case "C":
				return climbingAgent(pos);
			case "N":
				return notClimbingAgent(pos);
			case "F":
				return flyingAgent(pos);	
			case "W":
				return water(pos);
			default:
				throw new InvalidDataFileException("Unknown Simulator Object");
		}			
	}
	
	public SimulatorObj air(Pos2d pos){
		return new Air(pos);
	}
	
	public SimulatorObj terrain(Pos2d pos){
		SimulatorObj terrain= new Terrain(pos);
		//terrain.addPhysics(new FallDown());
		return terrain;
	}
	
	public SimulatorObj water(Pos2d pos){
		SimulatorObj water= new Water(pos);
		//water.addPhysics(new FallDown());
		water.addPhysics(new Spread(Config.SPREAD_TIME));
		return water;
	}
	
	private SimulatorObj flyingAgent(Pos2d pos){
		SimulatorObj agent= new NormalAgent(pos);
		agent.addCapability(new MoveLeft());
		agent.addCapability(new MoveRight());
		agent.addCapability(new MoveUp());
		agent.addCapability(new MoveDown());
		agent.addCapability(new InteractLeft());
		agent.addCapability(new InteractRight());
		agent.addCapability(new PickUpBottom());
		agent.addCapability(new Destroy());
		agent.addCapability(new DoNothing());
		return agent;
	}
	
	private SimulatorObj notClimbingAgent(Pos2d pos){
		SimulatorObj agent= new NormalAgent(pos);
		agent.addCapability(new MoveLeft());
		agent.addCapability(new MoveRight());
		agent.addCapability(new InteractLeft());
		agent.addCapability(new InteractRight());
		agent.addCapability(new PickUpBottom());
		agent.addCapability(new Destroy());
		agent.addCapability(new DoNothing());
		agent.addPhysics(new FallDown());
		return agent;
	}
	
	private SimulatorObj climbingAgent(Pos2d pos){
		SimulatorObj agent= new NormalAgent(pos);
		agent.addCapability(new MoveLeftClimb());
		agent.addCapability(new MoveRightClimb());
		agent.addCapability(new InteractLeft());
		agent.addCapability(new InteractRight());
		agent.addCapability(new PickUpBottom());
		agent.addCapability(new Destroy());
		agent.addCapability(new DoNothing());
		agent.addPhysics(new FallDown());
		return agent;
	}

}
