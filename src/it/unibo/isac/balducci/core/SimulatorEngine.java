package it.unibo.isac.balducci.core;

import it.unibo.isac.balducci.graphics.SimulatorGUI;
import it.unibo.isac.balducci.model.*;
import it.unibo.isac.balducci.monitors.PauseMonitor;

public class SimulatorEngine extends Thread{

	int nSteps;
	SimulatorGUI scene;
	World world;
	PauseMonitor pauseMonitor;
	Boolean run;
	FileLogger logger;
	
	public SimulatorEngine(int nSteps, SimulatorGUI scene, World world){
		this.nSteps=nSteps;
		this.scene=scene;
		this.world=world;
		run=true;
		pauseMonitor = new PauseMonitor();
		logger = FileLogger.getInstance();
	}
	
	public void run(){
		logger.newLogFile();
		for(int i=0; i<nSteps && run; i++){
			pauseMonitor.isPaused();
			world.doOneStep();
			scene.render();
			/*try {
				Thread.sleep(200);
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}*/
		}
	}

	
	public void terminate() {
		run=false;		
	}

	public void pauseSimulation() {
		pauseMonitor.pause();
		
	}

	public void resumeSimulation() {
		pauseMonitor.resume();		
	}
}
