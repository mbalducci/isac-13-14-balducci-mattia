package it.unibo.isac.balducci.core;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Calendar;

public class FileLogger {

	int count=0;
    File file;
	
	static private FileLogger instance;
		
	static public FileLogger getInstance(){
		if (instance == null){
			instance = new FileLogger();
		}
		return instance;
	}
	
	public void newLogFile(){
		String name = String.valueOf(count);
		file = new File("" + name + ".txt");

		while(file.exists()){
			count++;
		     name = String.valueOf(count);
		     file = new File("" + name + ".txt");
		} 
		
		if(!file.exists()) {
			try {	
			    file.createNewFile();				    
	
			    System.out.println("Log File Created");
	
			}catch (IOException e){
			    e.printStackTrace();
			}
		}
    }
	
	public void log(String msg){
		try{
			FileWriter fw = new FileWriter(file,true);
			BufferedWriter bw = new BufferedWriter(fw);
			bw.write(msg);
			bw.close();
		}
		catch(IOException e){
			e.printStackTrace();
		}
	    
	}
	
}
