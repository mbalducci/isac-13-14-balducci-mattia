package it.unibo.isac.balducci.exceptions;

public class InvalidDataFileException extends Exception {
	public InvalidDataFileException(String log) {
		super(log);
	}
}
