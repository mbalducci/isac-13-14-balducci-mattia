package it.unibo.isac.balducci.common;

import java.io.Serializable;

public class Pos2d implements Serializable{
	
	int row;
	int col;	
	
	public Pos2d(int row, int col){
		this.row=row;
		this.col=col;				
	}
	
	public int getRow(){
		return row;
	}
	
	public int getCol(){
		return col;
	}	

	@Override
	public boolean equals(Object pos2) {
		if(pos2 instanceof Pos2d){
			int row2= ((Pos2d)pos2).getRow();
			int col2= ((Pos2d)pos2).getCol();
			if(row==row2 && col==col2){
				return true;
			}
		}
		return false;
	}

	@Override
	public int hashCode() {
		return Integer.parseInt(""+row+col);
	}
	
	
	
}
