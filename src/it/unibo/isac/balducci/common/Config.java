package it.unibo.isac.balducci.common;

public class Config {

	public static final int N_ITERATIONS = 1000;
	public static final int N_SAMPLES = 1000;
	
	public static final int SEQUENCE_LENGTH = 15;
	public static final int SEQUENCE_MIN_LENGTH = 5;
	public static final double SEQUENCE_LENGTH_RANDOM_PROBL = 0.8;
	
	public static final int SPREAD_TIME = 5;
}
