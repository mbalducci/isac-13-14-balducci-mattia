package it.unibo.isac.balducci.physics;


import it.unibo.isac.balducci.common.Pos2d;
import it.unibo.isac.balducci.core.SimulatorFactory;
import it.unibo.isac.balducci.model.*;

public class Spread implements PhysicsComponent{

	int timer;
	int counter;
	
	public Spread(int timer){
		this.timer=timer;
		counter=0;		
	}

	@Override
	public void applyPhysics(World world, SimulatorObj obj) {
		if(counter==timer){			
			counter=0;
			Pos2d pos = obj.getPos();
			Pos2d spreadPos;
			
			//Spread Right
			spreadPos = new Pos2d(pos.getRow(),pos.getCol()+1);
			doSpread(world,obj,spreadPos);
			
			//Spread Left
			spreadPos = new Pos2d(pos.getRow(),pos.getCol()-1);
			doSpread(world,obj,spreadPos);
			
			//Spread Down
			spreadPos = new Pos2d(pos.getRow()+1,pos.getCol());
			doSpread(world,obj,spreadPos);			
		}
		else{
			counter++;
		}
	}
	
	private void doSpread(World world, SimulatorObj obj, Pos2d pos){
		SimulatorFactory f = SimulatorFactory.getInstance();
		if(world.getSimulatorObj(pos.getRow(),pos.getCol()) != null 
				&& world.getSimulatorObj(pos.getRow(),pos.getCol()).getType() == SimulatorObj.Type.AIR){	
			world.setSimulatorObj(pos, f.water(pos));			
		}
	}
}
