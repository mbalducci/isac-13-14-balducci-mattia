package it.unibo.isac.balducci.physics;

import java.io.Serializable;

import it.unibo.isac.balducci.model.*;

public interface PhysicsComponent extends Serializable{
	public void applyPhysics(World world, SimulatorObj obj);
}
