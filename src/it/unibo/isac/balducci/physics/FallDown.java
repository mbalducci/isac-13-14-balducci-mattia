package it.unibo.isac.balducci.physics;

import it.unibo.isac.balducci.common.Pos2d;
import it.unibo.isac.balducci.model.Air;
import it.unibo.isac.balducci.model.SimulatorObj;
import it.unibo.isac.balducci.model.World;

public class FallDown implements PhysicsComponent{

	@Override
	public void applyPhysics(World world, SimulatorObj obj) {
		Pos2d pos=obj.getPos();
		Pos2d oldPos;
		if(world.getSimulatorObj(pos.getRow()+1,pos.getCol()) != null 
				&& (world.getSimulatorObj(pos.getRow()+1,pos.getCol()).getType() != SimulatorObj.Type.TERRAIN)){		
				
			oldPos=pos;
			pos = new Pos2d(pos.getRow()+1, pos.getCol());
			obj.setPos(pos);
			if(obj.getType()!=SimulatorObj.Type.AGENT){
				world.setSimulatorObj(pos, obj);
				world.setSimulatorObj(oldPos, new Air(oldPos));
			}
		}			
	}

	
}
